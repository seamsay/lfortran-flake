{
  description = "A modern Fortran compiler built on top of LLVM.";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = {
    self,
    flake-utils,
    nixpkgs,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
      lib = pkgs.lib;
      callPackage = pkgs.callPackage;
    in {
      packages = {
        default = self.packages.${system}.lfortran-0_19;
        nightly = self.packages.${system}.lfortran-0_19_0-63;

        # See
        # - https://lfortran.github.io/tarballs/data.json
        # - https://github.com/lfortran/tarballs
        # for the possible versions that can be built.

        lfortran-0_18 = callPackage ./lfortran.nix {
          version = "0.18.0";
          srcHash = "sha256-BGz2Q8E+Ybljocs/rwG65bprsYGMsD3KPDmrVUTyYV4=";
        };

        lfortran-0_19 = callPackage ./lfortran.nix {
          version = "0.19.0";
          srcHash = "sha256-BH+eN0ZtkThuLEBk6qI3ZxRuijm2r4iIrylHrKFOCa4=";
        };

        lfortran-0_18_0-944 = callPackage ./lfortran.nix {
          version = "0.18.0";
          build = 944;
          commit = "0eddcab46";
          srcHash = "sha256-4oeZhFohFo2+iWxMjExtQEA7PcUxBUyGBKM35e5hQvY=";
        };

        lfortran-0_18_0-977 = callPackage ./lfortran.nix {
          version = "0.18.0";
          build = 977;
          commit = "e67f2f62d";
          srcHash = "sha256-MtxI93OoaLa0sTk/aLigAUd3Aq7TrgGqA311mbHP9qY=";
        };

        lfortran-0_19_0-63 = callPackage ./lfortran.nix {
          version = "0.19.0";
          build = 63;
          commit = "7e292f7aa";
          srcHash = "sha256-UfjeF0Tsgn/cpi2cTUzOVedRnNK/061H4OkjsAiwbmM=";
        };
      };

      builder = import ./lfortran.nix;

      checks = lib.trivial.pipe (builtins.attrNames self.packages.${system}) [
        (map (name: {
          name = "linking-${name}";
          value = callPackage ./tests/linking {lfortran = self.packages.${system}.${name};};
        }))
        builtins.listToAttrs
      ];

      formatter = pkgs.alejandra;
    });
}
