{
  stdenv,
  lfortran,
}:
stdenv.mkDerivation {
  name = "lfortran-linking-test";
  meta.timeout = 60;
  buildCommand = ''
    ${lfortran}/bin/lfortran ${./main.f90}
    touch $out
  '';
}
