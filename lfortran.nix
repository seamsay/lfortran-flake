{
  lib,
  stdenv,
  clang,
  cmake,
  fetchzip,
  llvm_11,
  python3,
  makeBinaryWrapper,
  zlib,
  version,
  srcHash,
  build ? null,
  commit ? null,
}: let
  pname = "lfortran";
  hash = srcHash;
  url =
    if build == null && commit == null
    then "https://lfortran.github.io/tarballs/release/${pname}-${version}.tar.gz"
    else if build != null && commit != null
    then "https://lfortran.github.io/tarballs/dev/${pname}-${version}-${builtins.toString build}-g${commit}.tar.gz"
    else if build == null && commit != null
    then throw "Got a commit hash (${commit}) but no build number."
    else if build != null && commit == null
    then throw "Got a build number (${build}) but no commit hash."
    else throw "Nix has broken logic...";
in
  stdenv.mkDerivation {
    inherit pname version;

    src = fetchzip {
      inherit url hash;
    };

    nativeBuildInputs = [cmake makeBinaryWrapper];
    buildInputs = [clang llvm_11 python3 zlib.static];

    cmakeFlags = ["-DWITH_LLVM=YES"];

    # TODO: LFortran always seems to look in `lib` rather than `share/lfortran/lib`, so we patch out any references to it.
    patches =
      if (version == "0.18.0" && build == null) || (version == "0.18.0" && build <= 976)
      then [
        ./patches/install-libraries-in-lib/0.18.0.patch
      ]
      else if (version == "0.18.0" && build > 976)
      then [
        ./patches/install-libraries-in-lib/0.18.0_977-.patch
      ]
      else if (version == "0.19.0")
      then [
        ./patches/install-libraries-in-lib/0.19.0.patch
      ]
      else [];

    doCheck = true;
    checkTarget = "test";

    postFixup = ''
      wrapProgram $out/bin/lfortran \
        --suffix PATH : "${lib.makeBinPath [clang]}"
    '';

    meta = {
      homepage = "https://lfortran.org/";
      description = "A modern Fortran compiler built on top of LLVM.";
      license = lib.licenses.bsd3;
    };
  }
